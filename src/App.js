import React from "react";
import "./App.css";
import { choleryk, flegmatyk, melancholik, sangwinik } from "./testAnswers";
import { checkboxes_questions } from "./testQuestions";

function App() {
  const [selected, setSelected] = React.useState([]);
  // const [result, setResult] = React.useState();
  const [renderedResult, setRenderedResult] = React.useState();

  // Render all checkboxes with description textes
  function renderCheckboxes() {
    let checkboxes = [];
    for (let i = 0; i < 232; i++) {
      checkboxes.push(
        <div>
          <input
            type={"checkbox"}
            key={i}
            value={i + 1}
            checked={selected[i]}
            onChange={(a) => {
              let newList = [...selected];
              newList[i] = a.target.checked;
              setSelected(newList);
            }}
            id={checkboxes_questions[i]}
          />
          <label for={checkboxes_questions[i]}>
            {i + 1}. {checkboxes_questions[i]}
          </label>
        </div>
      );
    }
    return checkboxes;
  }
  // Generate answer
  function generateResult() {
    let output = {
      choleryk: 0,
      sangwinik: 0,
      melancholik: 0,
      flegmatyk: 0,
    };

    let bug = selected.map((selectedValue, index) => {
      if (selectedValue === true) {
        choleryk.map((cholerykValue) => {
          if (index + 1 === cholerykValue) {
            output.choleryk += 1; // licz ilosc trafien dla choleryka
          }
          return null;
        });
        sangwinik.map((sangwinikValue) => {
          if (index + 1 === sangwinikValue) {
            output.sangwinik += 1; // licz ilosc trafien dla sangwinika
          }
          return null;
        });
        melancholik.map((melancholikValue) => {
          if (index + 1 === melancholikValue) {
            output.melancholik += 1; // licz ilosc trafien dla melancholika
          }
          return null;
        });
        flegmatyk.map((flegmatykValue) => {
          if (index + 1 === flegmatykValue) {
            output.flegmatyk += 1; // licz ilosc trafien dla flegmatyka
          }
          return null;
        });
      }
      return null;
    });
    console.log(bug);
    // setResult(output);
    setRenderedResult(
      <>
        <ul>
          <li>Choleryk: {output.choleryk}</li>
          <li>Sangwinik: {output.sangwinik}</li>
          <li>Melancholik: {output.melancholik}</li>
          <li>Flegmatyk: {output.flegmatyk}</li>
        </ul>
      </>
    );
  }
  // Set 'selected' variable, all 232 positions to 'false'
  React.useEffect(() => {
    let defaultArray = [];
    for (let i = 0; i < 232; i++) {
      defaultArray.push(false);
    }
    setSelected(defaultArray);
  }, []);

  return (
    <div className="container">
      <h1>Test Temperamentu</h1>
      <div className="checkboxes">{renderCheckboxes()}</div>
      <button
        onClick={() => {
          generateResult();
        }}
      >
        Check
      </button>
      {renderedResult}
    </div>
  );
}

export default App;
